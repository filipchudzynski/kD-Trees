
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import java.awt.Color;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Filip
 */
public class KdTree {

    private TwodTree t;

    public KdTree()// construct an empty set of points 
    {
        t = new TwodTree();
    }

    public boolean isEmpty() // is the set empty? 
    {
        return t.isEmpty();
    }

    public int size() // number of points in the set 
    {
        return t.size();
    }

    public void insert(Point2D p) // add the point to the set (if it is not already in the set)
    {
        t.insert(p);
    }

    public boolean contains(Point2D p) // does the set contain point p? 
    {
        return t.contains(p);
    }

    public void draw() // draw all points to standard draw 
    {
        t.draw();

    }

    public Iterable<Point2D> range(RectHV rect) // all points that are inside the rectangle
    {
        return t.range(rect);
    }

    public Point2D nearest(Point2D p) // a nearest neighbor in the set to point p; null if the set is empty 
    {
        return t.nearest(p);
    }

    private class TwodTree {

        Node root;
        int size;

        TwodTree()// construct an empty set of points 
        {
            size = 0;
        }

        public boolean isEmpty() // is the set empty? 
        {
            return root == null;
        }

        public int size() // number of points in the set 
        {
            return size;
        }

        public void insert(Point2D p) // add the point to the set (if it is not already in the set)
        {
            if (p == null) {
                throw new IllegalArgumentException();
            }
            if (isEmpty()) {
                root = new Node();
                root.p = p;
                root.rect = new RectHV(0, 0, 1, 1);
            } else if (contains(p)) {
                return;
            } else {
                insert(p, true, root);
            }
            ++size;

        }

        private void insert(Point2D p, boolean horizontal, Node n) {
            if (horizontal) {
                if (Point2D.X_ORDER.compare(p, n.p) < 0) {
                    if (n.left == null) {
                        n.left = new Node();
                        n.left.p = p;
                        n.left.rect = new RectHV(n.rect.xmin(), n.rect.ymin(), n.p.x(), n.rect.ymax());
                        return;
                    }
                    insert(p, false, n.left);
                } else {
                    if (n.right == null) {
                        n.right = new Node();
                        n.right.p = p;
                        n.right.rect = new RectHV(n.p.x(), n.rect.ymin(), n.rect.xmax(), n.rect.ymax());
                        return;
                    }
                    insert(p, false, n.right);
                }

            } else {
                if (Point2D.Y_ORDER.compare(p, n.p) < 0) {
                    if (n.left == null) {
                        n.left = new Node();
                        n.left.p = p;
                        n.left.rect = new RectHV(n.rect.xmin(), n.rect.ymin(), n.rect.xmax(), n.p.y());
                        return;
                    }
                    insert(p, true, n.left);
                } else {
                    if (n.right == null) {
                        n.right = new Node();
                        n.right.p = p;
                        n.right.rect = new RectHV(n.rect.xmin(), n.p.y(), n.rect.xmax(), n.rect.ymax());
                        return;
                    }
                    insert(p, true, n.right);
                }

            }
        }

        public boolean contains(Point2D p) // does the set contain point p? 
        {
            if (p == null) {
                throw new IllegalArgumentException();
            }
            Node n = root;
            boolean horizontal = true;
            while (n != null) {
                if (horizontal) {
                    if (p.equals(n.p)) {
                        return true;
                    } else if (Point2D.X_ORDER.compare(p, n.p) < 0) {
                        n = n.left;
                        horizontal = false;
                    } else {
                        n = n.right;
                        horizontal = false;
                    }
                } else if (p.equals(n.p)) {
                    return true;
                } else if (Point2D.Y_ORDER.compare(p, n.p) < 0) {
                    n = n.left;
                    horizontal = true;
                } else {
                    n = n.right;
                    horizontal = true;
                }
            }
            return false;
        }

        public void draw() {
            draw(root);
        }

        private void draw(Node n) {
            if (n.left != null) {
                draw(n.left);
            }
            if (n.right != null) {
                draw(n.right);
            }
            n.p.draw();
            n.rect.draw();
        }

        public Bag<Point2D> range(RectHV rect) {
            if (rect == null) {
                throw new IllegalArgumentException();
            }
            Bag<Point2D> container = new Bag<Point2D>();
            range(root, rect, container);
            return container;
        }

        private void range(Node n, RectHV r, Bag<Point2D> b) {
            if (r.contains(n.p)) {
                b.add(n.p);
            }
            if (n.left != null && n.left.rect.intersects(r)) {
                range(n.left, r, b);
            }
            if (n.right != null && n.right.rect.intersects(r)) {
                range(n.right, r, b);
            }
        }

        public Point2D nearest(Point2D p) {
            Point2D closest = root.p;
            closest = nearest(root, p, closest, true);
            return closest;
        }

        private Point2D nearest(Node n, Point2D p, Point2D closest, boolean horizontal) {
            if (n.p.distanceSquaredTo(p) < closest.distanceSquaredTo(p)) {
                closest = n.p;
            }

            if (n.left != null && closest.distanceSquaredTo(p) > n.left.rect.distanceSquaredTo(p)
                    && n.right != null && closest.distanceSquaredTo(p) > n.right.rect.distanceSquaredTo(p)) {
                if (horizontal && Point2D.X_ORDER.compare(p, n.p) < 0) {
                    closest = nearest(n.left, p, closest, !horizontal);
                    closest = nearest(n.right, p, closest, !horizontal);
                } else if (!horizontal && Point2D.Y_ORDER.compare(p, n.p) < 0) {
                    closest = nearest(n.left, p, closest, !horizontal);
                    closest = nearest(n.right, p, closest, !horizontal);
                }
            } else if (n.left != null && (closest.distanceSquaredTo(p) > n.left.rect.distanceSquaredTo(p))) {
                closest = nearest(n.left, p, closest, !horizontal);
            } else if (n.right != null && closest.distanceSquaredTo(p) > n.right.rect.distanceSquaredTo(p)) {
                closest = nearest(n.right, p, closest, !horizontal);
            }
            StdDraw.setPenColor(StdDraw.BLUE);
            closest.draw();
            return closest;
        }
    }

    public static void main(String[] args) // unit testing of the methods (optional) 
    {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.1, 0.5));
        tree.insert(new Point2D(0.3, 0.4));
        tree.insert(new Point2D(0.2, 0.1));
        tree.insert(new Point2D(0.6, 0.7));
        tree.insert(new Point2D(0.5, 0.3));
        //    tree.printLeft();
        //StdOut.println(tree.contains(new Point2D(0, 3)));
        StdDraw.setScale();
        StdDraw.setPenColor(Color.red);
        StdDraw.setPenRadius(0.01);
        tree.draw();
        Iterable<Point2D> i = tree.range(new RectHV(0.0, 0.0, 0.3, 1.0));
        for (Point2D a : i) {
            StdOut.println(a.toString());
        }

    }

    private static class Node {

        Point2D p;
        Node left, right;
        RectHV rect;
    }
}
