
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdOut;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Filip
 */
public class PointSET {

    private SET<Point2D> s;

    public PointSET() // construct an empty set of points 
    {
        s = new SET<>();
    }

    public boolean isEmpty() // is the set empty? 
    {
        return s.isEmpty();
    }

    public int size() // number of points in the set 
    {
        return s.size();
    }

    public void insert(Point2D p) // add the point to the set (if it is not already in the set)
    {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        s.add(p);
    }

    public boolean contains(Point2D p) // does the set contain point p? 
    {   if (isEmpty())
            return false;
        if (p == null) {
            throw new IllegalArgumentException();
        }
        return s.contains(p);
    }

    public void draw() // draw all points to standard draw 
    {
        if(isEmpty())
            return;
        for (Point2D p : s) {
            p.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) // all points that are inside the rectangle 
    {
        if(isEmpty())
            return null;
        if (rect == null) {
            throw new IllegalArgumentException();
        }
        Bag<Point2D> b = new Bag<>();
        for (Point2D p : s) {
            if (rect.contains(p)) {
                b.add(p);
            }
        }
        return b;
    }

    public Point2D nearest(Point2D p) // a nearest neighbor in the set to point p; null if the set is empty 
    {
        if(isEmpty())
            return null;
        Point2D toReturn = s.min();
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (s.isEmpty()) {
            return null;
        }
       for(Point2D q:s){
           if(q.distanceSquaredTo(p)<toReturn.distanceSquaredTo(p))
               toReturn = q;
       }
       return toReturn;
    }

    public static void main(String[] args) // unit testing of the methods (optional) 
    {
        
    }
}
